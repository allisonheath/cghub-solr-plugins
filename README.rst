CGHub SOLR Plugins
==================

A selection of Java classes that can be used to customize a SOLR instance for use at CGHub.

Prerequisites
-------------

* JDK
* Maven

Build
-----

::

   mvn package

Deploy
------

::

   rsync target/cghub-solr-plugins-1.0-SNAPSHOT.jar app01:/opt/SOLR/home/lib

Configure
---------

To activate the plugins, use solrconfig.xml and schema.xml.